package fakta.ayu.appx08a

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var db : SQLiteDatabase
    lateinit var fragProdi : FragmentProdi
    lateinit var fragMhs : FragmentMahasiswa
    lateinit var ft : FragmentTransaction

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_option,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){

        }
        return super.onOptionsItemSelected(item)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragProdi = FragmentProdi()
        fragMhs = FragmentMahasiswa()
        db = DBOpenHelper(this).writableDatabase
    }

    fun getDBObject() : SQLiteDatabase{
        return db
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when(p0.itemId){
            R.id.itemProdi ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragProdi).commit()
                frameLayout.setBackgroundColor(Color.argb(245, 255, 255, 225))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemMhs ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragMhs).commit()
                frameLayout.setBackgroundColor(Color.argb(245, 225, 255, 255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemAbout -> frameLayout.visibility = View.GONE
        }
        return true
    }
}